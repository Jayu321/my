package pomclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Awesomeproductdesignbyartist {
	
	@FindBy(xpath="//input[@placeholder='Search designs and products']")
	private WebElement searchbar;
	
	/*@FindBy (xpath="//button[@data-testid='ds-typeahead-search-button']")
	private WebElement searchbarsymbol;*/
	
	@FindBy (xpath="(//div[@data-testid='ds-box']//ul//div)[4]")
	private WebElement sportsoption;
	
	private WebDriver driver;
	
	public  Awesomeproductdesignbyartist(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void entertextinsearchbar(String name)
	{
		searchbar.sendKeys(name);
	}
	
	/*public void clickonsearchbarsymbol()
	{
		searchbarsymbol.click();
	}*/
	
	public void selectsportoptionfromlist()
	{
		sportsoption.click();
	}
	
	
	

}
