package pomclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Sportssteakersale {

	@FindBy(xpath="(//div[@data-testid='ds-box']//button)[4]")
	private WebElement categoryStickers;
	
	@FindBy (xpath="(//div[@data-testid='ds-box']//label)[3]")
	private WebElement FinishTransperent;
	
	@FindBy (xpath="(//div[@data-testid='ds-box']//label)[5]")
	private WebElement Dubledollarsymbol;
	
	@FindBy (xpath="(//div[@data-testid='ds-box']//label)[10]")
	private WebElement ArtworkDrawing;
	
	private WebDriver driver;
	public Sportssteakersale(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectStickersfromcategory()
	{
		categoryStickers.click();
	}
	
	public void selectTransperentfromFinish()
	{
		FinishTransperent.click();
	}
	
	public void selectDubledollarsymbol()
	{
		Dubledollarsymbol.click();
	}
	
	public void selectArtworkDrawing()
	{
		ArtworkDrawing.click();
	}
	
}
