package TestClasses;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pomclasses.Awesomeproductdesignbyartist;
import pomclasses.Sportssteakersale;

public class newdoodle {
	
	
	private WebDriver driver;
	private Awesomeproductdesignbyartist searchproduct;
	private Sportssteakersale filter;
	
	@Parameters("multibrowsers")
	@BeforeTest
	public void launchbrowser(String browser)
	{
		if(browser.equals("chrome"))
		{
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Admin\\chromedriver.exe");
	    driver= new ChromeDriver();
		}
		
		if (browser.equals("edge"))
		{
			System.setProperty("webdriver.edge.driver","C:\\Users\\Admin\\Downloads\\edgedriver_win64\\msedgedriver.exe");
		    driver= new EdgeDriver();
		}
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS
			);
	}
	
	@BeforeClass
	public void storeobject()
	{
	 searchproduct=new Awesomeproductdesignbyartist(driver);
	 filter=new Sportssteakersale(driver);
	}

	@BeforeMethod
	public void luanchurl() throws InterruptedException
	{
		driver.get("https://www.redbubble.com/");
		Thread.sleep(2000);
	}
	
	@Test
	public void verifyselectproduct() throws InterruptedException
	{
		searchproduct.entertextinsearchbar("sports");
		Thread.sleep(2000);
		searchproduct.selectsportoptionfromlist();
		Thread.sleep(2000);
	}
	
	@Test
	public void filterselectproduct() throws InterruptedException
	{
		filter.selectStickersfromcategory();
		Thread.sleep(2000);
		filter.selectTransperentfromFinish();
		Thread.sleep(2000);
		filter.selectDubledollarsymbol();
		Thread.sleep(2000);
		filter.selectArtworkDrawing();
		Thread.sleep(2000);
	}
	
	@AfterClass
	public void cleanobjectfornextbrowser()
	{
		searchproduct=null;
		filter=null;	
	}
	
	@AfterTest
	public void closebrowser()
	{
		driver.close();
		driver.quit();
		driver=null;
		System.gc();
	}
}
